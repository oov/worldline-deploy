<?php
abstract class Authorizer {
	protected $client_id;
	protected $client_secret;
	protected $callback_uri;
	public function __construct($client_id, $client_secret, $callback_uri){
		$this->client_id = $client_id;
		$this->client_secret = $client_secret;
		$this->callback_uri = $callback_uri;
	}
	abstract public function auth();
}

class HeelloAuthorizer extends Authorizer {
	private $auth_uri = 'https://api.heello.com/oauth/authorize';
	private $token_uri = 'https://api.heello.com/oauth/token';
	private $me_uri = 'https://api.heello.com/users/me.json';

	private $errors = array(
		"access_denied" => "Access to the account was denied by the user",
		"invalid_request" => "Invalid grant_type parameter or parameter missing",
		"invalid_client" => "Client is invalid",
		"unauthorized_client" => "Client is not authorized",
		"redirect_uri_mismatch" => "Redirect URI does not match domain on record",
		"unsupported_response_type" => "Response type not recognized",
		"invalid_scope" => "Scope not recognized",
		"invalid_grant" => "Grant type not recognized",
		"unsupported_grant_type" => "Grant type not supported",
		"invalid_token" => "Access token provided is invalid",
		"expired_token" => "Access token provided is expired",
		"insufficient_scope" => "Scope insufficient for requested action",
		"ssl_required" => "Requests must be made over SSL",
	);

	private function error2Msg($error){
		return isset($this->errors[$error]) ? $this->errors[$error] : $error;
	}

	public function __construct($client_id, $client_secret, $callback_uri){
		parent::__construct($client_id, $client_secret, $callback_uri);
		require_once 'Zend/Http/Client.php';
	}

	public function auth(){
		$code = @$_GET["code"];
		if ($code) {
			$client = new Zend_Http_Client($this->token_uri);
			$client->setMethod(Zend_Http_Client::POST);
			$client->setHeaders('Accept', 'application/vnd.heello.v2');
			$client->setParameterPost(array(
				'client_id' => $this->client_id,
				'client_secret' => $this->client_secret,
				'redirect_uri' => $this->callback_uri,
				'code' => $code,
				'response_type' => 'token',
				'grant_type' => 'authorization_code',
			));
			$response = $client->request();
			if ($response->isError()){
				throw new Exception(sprintf(
					'Cannot verify access token[%d %s] %s',
					$response->getStatus(),
					$response->getMessage(),
					$response->getBody()
				));
			}

			$token = json_decode($response->getBody(), true);
			$me = $this->me($token);
			$ret = array(
				'access_token' => $token['access_token'],
				'refresh_token' => $token['refresh_token'],
				'id' => $me["response"]['id'],
				'name' => $me["response"]['username'],
			);
			if (isset($me['avatar']) && isset($me['avatar']['url'])){
				$ret['image'] = $me['avatar']['url'];
			}
			return $ret;
		}

		$error = @$_GET["error"];
		if($error) {
			throw new Exception($this->error2Msg($error));
		}

		header(sprintf(
			"Location: %s?%s",
			$this->auth_uri,
			http_build_query(array(
				'client_id' => $this->client_id,
				'response_type' => 'code',
				'redirect_uri' => $this->callback_uri,
			))
		));
		exit;
	}

	private function me(&$token){
		$client = new Zend_Http_Client($this->me_uri);
		$response = $this->requestWithRefresh($token, $client);
		return json_decode($response->getBody(), true);
	}

	private function requestWithRefresh(&$token, $client){
		$client->setParameterGet('access_token', $token['access_token']);
		$response = $client->request();
		if ($response->getStatus() == 401 && json_decode($response->getBody())->error == 'expired_token'){
			$refresh_client = new Zend_Http_Client($this->token_uri);
			$refresh_client->setMethod(Zend_Http_Client::POST);
			$refresh_client->setParameterPost(array(
				'client_id' => $this->client_id,
				'client_secret' => $this->client_secret,
				'redirect_uri' => $this->callback_uri,
				'response_type' => 'token',
				'refresh_token' => $token['refresh_token'],
				'grant_type' => 'refresh_token',
			));
			$refresh_response = $refresh_client->request();
			if ($refresh_response->isError()){
				throw new Exception(sprintf(
					'Cannot refresh token [%d %s] %s',
					$refresh_response->getStatus(),
					$refresh_response->getMessage(),
					$refresh_response->getBody()
				));
			}
			$new_token = json_decode($refresh_response->getBody(), true);
			$token['access_token'] = $new_token['access_token'];
			$token['refresh_token'] = $new_token['refresh_token'];

			$client->setParameterGet('access_token', $token['access_token']);
			$response = $client->request();
		}

		if ($response->isError()){
			throw new Exception(sprintf(
				'Cannot complete request [%d %s] %s',
				$response->getStatus(),
				$response->getMessage(),
				$response->getBody()
			));
		}

		return $response;
	}
}

class TwitterAuthorizer extends Authorizer {
	private $consumer;
	private $oauth_config;

	public function __construct($client_id, $client_secret, $callback_uri){
		parent::__construct($client_id, $client_secret, $callback_uri);
		require_once 'Zend/Http/Client.php';
		require_once 'Zend/Oauth.php';
		require_once 'Zend/Oauth/Token.php';
		require_once 'Zend/Oauth/Consumer.php';
		$this->oauth_config = array(
			'requestScheme' => Zend_Oauth::REQUEST_SCHEME_HEADER,
			'signatureMethod' => 'HMAC-SHA1',
			'callbackUrl' => $this->callback_uri,
			'requestTokenUrl' => 'http://twitter.com/oauth/request_token',
			'authorizeUrl' => 'http://twitter.com/oauth/authenticate',
			'accessTokenUrl' => 'http://twitter.com/oauth/access_token',
			'consumerKey' => $this->client_id,
			'consumerSecret' => $this->client_secret,
		);
		$this->consumer = new Zend_Oauth_Consumer($this->oauth_config);
	}

	public function auth(){
		if (isset($_GET['oauth_token']) && isset($_SESSION['TWITTER_REQUEST_TOKEN'])) {
			$token = $this->consumer->getAccessToken($_GET, unserialize($_SESSION['TWITTER_REQUEST_TOKEN']));
		  unset($_SESSION['TWITTER_REQUEST_TOKEN']);

			//認証に成功したのでユーザー情報を拾ってくる
			$c = $token->getHttpClient($this->oauth_config);
			$c->setUri('https://api.twitter.com/1/users/show.json');
			$c->setParameterGet('user_id', $token->getParam('user_id'));
			$c->setParameterGet('include_entities', 0);
			$resp = $c->request();
			$me = json_decode($resp->getBody(), true);

			$ret = array(
				'token' => $token->getToken(),
				'token_secret' => $token->getTokenSecret(),
				'id' => $me['id_str'],
				'name' => $me['name'],
			);
			if (isset($me['profile_image_url'])){
				$ret['image'] = $me['profile_image_url'];
			}
			return $ret;
		}

		$token = $this->consumer->getRequestToken();
		$_SESSION['TWITTER_REQUEST_TOKEN'] = serialize($token);
		$this->consumer->redirect();
		exit;
	}
}

class GoogleAuthorizer extends Authorizer {
	private $auth_uri = 'https://accounts.google.com/o/oauth2/auth';
	private $token_uri = 'https://accounts.google.com/o/oauth2/token';
	private $me_uri = 'https://www.googleapis.com/oauth2/v1/userinfo';

	private $errors = array(
		"access_denied" => "Access to the account was denied by the user",
		"invalid_request" => "Invalid grant_type parameter or parameter missing",
		"invalid_client" => "Client is invalid",
		"unauthorized_client" => "Client is not authorized",
		"redirect_uri_mismatch" => "Redirect URI does not match domain on record",
		"unsupported_response_type" => "Response type not recognized",
		"invalid_scope" => "Scope not recognized",
		"invalid_grant" => "Grant type not recognized",
		"unsupported_grant_type" => "Grant type not supported",
		"invalid_token" => "Access token provided is invalid",
		"expired_token" => "Access token provided is expired",
		"insufficient_scope" => "Scope insufficient for requested action",
		"ssl_required" => "Requests must be made over SSL",
	);

	private function error2Msg($error){
		return isset($this->errors[$error]) ? $this->errors[$error] : $error;
	}

	public function __construct($client_id, $client_secret, $callback_uri){
		parent::__construct($client_id, $client_secret, $callback_uri);
		require_once 'Zend/Http/Client.php';
	}

	public function auth(){
		$code = @$_GET["code"];
		if ($code) {
			$client = new Zend_Http_Client($this->token_uri);
			$client->setMethod(Zend_Http_Client::POST);
			$client->setParameterPost(array(
				'code' => $code,
				'client_id' => $this->client_id,
				'client_secret' => $this->client_secret,
				'redirect_uri' => $this->callback_uri,
				'grant_type' => 'authorization_code',
			));
			$response = $client->request();
			if ($response->isError()){
				throw new Exception(sprintf(
					'Cannot verify access token[%d %s] %s',
					$response->getStatus(),
					$response->getMessage(),
					$response->getBody()
				));
			}

			$token = json_decode($response->getBody(), true);
			$me = $this->me($token);
			$ret = array(
				'access_token' => $token['access_token'],
				'id' => $me['id'],
				'name' => $me['name'],
			);
			if (isset($me['picture'])){
				$ret['image'] = $me['picture'];
			}
			return $ret;
		}

		$error = @$_GET["error"];
		if($error) {
			throw new Exception($this->error2Msg($error));
		}

		header(sprintf(
			"Location: %s?%s",
			$this->auth_uri,
			http_build_query(array(
				'response_type' => 'code',
				'client_id' => $this->client_id,
				'redirect_uri' => $this->callback_uri,
				'scope' => 'https://www.googleapis.com/auth/userinfo.profile',
			))
		));
		exit;
	}

	private function me(&$token){
		$client = new Zend_Http_Client($this->me_uri);
		$response = $this->requestWithRefresh($token, $client);
		return json_decode($response->getBody(), true);
	}

	private function requestWithRefresh(&$token, $client){
		//Googleのリフレッシュトークンはオフラインアクセス用APIだけっぽいが…
		$client->setParameterGet('access_token', $token['access_token']);
		$response = $client->request();
		if ($response->getStatus() == 401 && json_decode($response->getBody())->error == 'expired_token'){
			$refresh_client = new Zend_Http_Client($this->token_uri);
			$refresh_client->setMethod(Zend_Http_Client::POST);
			$refresh_client->setParameterPost(array(
				'client_id' => $this->client_id,
				'client_secret' => $this->client_secret,
				'redirect_uri' => $this->callback_uri,
				'response_type' => 'token',
				'refresh_token' => $token['refresh_token'],
				'grant_type' => 'refresh_token',
			));
			$refresh_response = $refresh_client->request();
			if ($refresh_response->isError()){
				throw new Exception(sprintf(
					'Cannot refresh token [%d %s] %s',
					$refresh_response->getStatus(),
					$refresh_response->getMessage(),
					$refresh_response->getBody()
				));
			}
			$new_token = json_decode($refresh_response->getBody(), true);
			$token['access_token'] = $new_token['access_token'];
			$token['refresh_token'] = $new_token['refresh_token'];

			$client->setParameterGet('access_token', $token['access_token']);
			$response = $client->request();
		}

		if ($response->isError()){
			throw new Exception(sprintf(
				'Cannot complete request [%d %s] %s',
				$response->getStatus(),
				$response->getMessage(),
				$response->getBody()
			));
		}

		return $response;
	}
}

class InstagramAuthorizer extends Authorizer {
	private $auth_uri = 'https://api.instagram.com/oauth/authorize/';
 	private $token_uri = 'https://api.instagram.com/oauth/access_token';
 	private $me_uri = 'https://www.googleapis.com/oauth2/v1/userinfo';

	public function __construct($client_id, $client_secret, $callback_uri){
		parent::__construct($client_id, $client_secret, $callback_uri);
		require_once 'Zend/Http/Client.php';
 	}

	public function auth(){
		$code = @$_GET["code"];
 		if ($code) {
 			$client = new Zend_Http_Client($this->token_uri);
 			$client->setMethod(Zend_Http_Client::POST);
 			$client->setParameterPost(array(
				'code' => $code,
				'client_id' => $this->client_id,
				'client_secret' => $this->client_secret,
				'redirect_uri' => $this->callback_uri,
				'grant_type' => 'authorization_code',
			));
			$response = $client->request();
			if ($response->isError()){
				throw new Exception(sprintf(
					'Cannot verify access token[%d %s] %s',
					$response->getStatus(),
					$response->getMessage(),
					$response->getBody()
				));
			}
			$token = json_decode($response->getBody(), true);
			$ret = array(
				'access_token' => $token['access_token'],
				'id' => $token['user']['id'],
				'name' => $token['user']['full_name'],
			);
			if (isset($token['user']['profile_picture'])){
				$ret['image'] = $token['user']['profile_picture'];
			}
			return $ret;
		}

		$error = @$_GET["error"];
		if($error) {
			throw new Exception("$_GET[error_reason]: $_GET[error_description]");
		}

		header(sprintf(
			"Location: %s?%s",
			$this->auth_uri,
			http_build_query(array(
				'response_type' => 'code',
				'client_id' => $this->client_id,
				'redirect_uri' => $this->callback_uri,
			))
		));
		exit;
	}
}


