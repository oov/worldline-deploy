<?php
$user_id = $_GET["user"];
$user = User::get(new MongoId($user_id));
if (strlen($user_id) != 24 || !ctype_xdigit($user_id) || !is_array($user)){
	header("Status: 404 Not Found");
	showError("ユーザーが見つかりませんでした。", "User not found");
	exit;
}

$is_mine =	$user['_id'] == $sess->user['_id'];

header("Content-type: text/html; charset=UTF-8");

$username = htmlspecialchars($user['name'], ENT_QUOTES, 'UTF-8');

$template = new HeadTemplate(HeadTemplate::TEMPLATE);
$template->title = "$user[name] さんのプロフィール - WORLDLINE";
$template->user = $sess->user;
echo $template->render();

class UserProfileTemplate extends Mustache {
	public $user;

	public function __construct($template = self::TEMPLATE, $view = null, $partials = null, array $options = null) {
		parent::__construct($template, $view, $partials, $options);
	}

	const TEMPLATE =<<<EOT
<div class="well profile">
	<div class="row-fluid">
		<div class="span2">
			<img src="{{#user.avatar}}/f{{user._id}}.jpg?{{user.avatar}}{{/user.avatar}}{{^user.avatar}}/img/avatar.png{{/user.avatar}}" alt="">
		</div>
		<div class="span10">
			<h1>{{user.name}}</h1>
			<p>{{#user.catch}}{{user.catch}}{{/user.catch}}{{^user.catch}}天は人の上に人を作らず（ｷﾘｯ{{/user.catch}}</p>
		</div>
	</div>
</div>
EOT;
}

$template = new UserProfileTemplate();
$template->user = $user;
echo $template->render();
?>
<h2><?php echo $username; ?>語録</h2>
<?php
	if ($is_mine){
		echo "<p>※自分自身のプロフィールなので忍者投稿も見えてます。</p>";
	}
?>
<ul class="unstyled">
<?php
	$tree = Worldline::getTreeCollection();
	$q = array(
		"user._id" => $user["_id"],
		"del" => array('$exists' => false)
	);
	if (!$is_mine){
		$q["ninja"] = array('$exists' => false);
	}
	$cursor = $tree->find($q)->sort(array("adddate" => -1))->limit(Worldline::LIMIT);
	foreach($cursor as $doc){
		echo '<li class="well">'.nl2br(htmlspecialchars($doc['title'], ENT_QUOTES, 'UTF-8')).'</li>';
	}
?>
</ul>
</div>
</html>
