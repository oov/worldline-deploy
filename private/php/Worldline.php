<?
require_once 'WorldlineSecure.php';
class Worldline {
	const WORLDLINE = 20;
	const LIMIT = 20;

	static private $db;

	static private $tree;
	static private $users;
	static private $fileGFS;

	static private $mongoSession;
	static private $sess;

	static public function getDomain(){
		return $_SERVER['HTTP_HOST'];
	}

	static public function getBaseURL(){
		return (Worldline::isHttps() ? 'https://' : 'http://') . self::getDomain() . '/';
	}

	static public function isHttps(){
		return @$_SERVER['HTTPS'] == 'on';
	}

	static public function isXmlHttpRequest(){
		return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH']  == 'XMLHttpRequest';
	}

	static public function getAuthCallbackUri(){
		return self::getBaseURL() . "?a=login";
	}

	static public function getDb(){
		return self::$db;
	}

	static public function getTreeCollection(){
		return self::$tree;
	}

	static public function getUsersCollection(){
		return self::$users;
	}

	static public function getFileGridFS(){
		return self::$fileGFS;
	}

	static public function getDefaultSession(){
		return self::$sess;
	}

	static public function getHeelloAuthorizer(){
		require_once 'Authorizer.php';
		return new HeelloAuthorizer(
			WorldlineSecure::HEELLO_CLIENT_ID,
			WorldlineSecure::HEELLO_CLIENT_SECRET,
			self::getAuthCallbackUri()
		);
	}

	static public function getTwitterAuthorizer(){
		require_once 'Authorizer.php';
		return new TwitterAuthorizer(
			WorldlineSecure::TWITTER_CLIENT_ID,
			WorldlineSecure::TWITTER_CLIENT_SECRET,
			self::getAuthCallbackUri()
		);
	}

	static public function getGoogleAuthorizer(){
		require_once 'Authorizer.php';
		return new GoogleAuthorizer(
			WorldlineSecure::GOOGLE_CLIENT_ID,
			WorldlineSecure::GOOGLE_CLIENT_SECRET,
			self::getAuthCallbackUri()
		);
	}

	static public function getInstagramAuthorizer(){
		require_once 'Authorizer.php';
		return new InstagramAuthorizer(
			WorldlineSecure::INSTAGRAM_CLIENT_ID,
			WorldlineSecure::INSTAGRAM_CLIENT_SECRET,
			self::getAuthCallbackUri()
		);
	}

	static public function init(){
		mb_internal_encoding("UTF-8");
		mb_regex_encoding("UTF-8");

		self::initMongo();
		self::initSession();
	}

	static private function initMongo(){
		$mongo = new Mongo();
		$db = $mongo->selectDB(WorldlineSecure::DATABASE_NAME);
		self::$db      = $db;
		self::$tree    = $db->tree;
		self::$users   = $db->users;
		self::$fileGFS = $db->getGridFS('file');
		
		//毎回やらなくてもいい
		self::$tree->ensureIndex(array("parent" => 1, "moddate" => -1));
		self::$tree->ensureIndex(array("adddate" => 1));
		self::$users->ensureIndex(array("from" => 1), array("unique" => true));
	}

	static private function initSession(){
		require_once 'MongoSession.php';
		require_once 'Zend/Session.php';
		self::$mongoSession = new MongoSession(WorldlineSecure::DATABASE_NAME, 'sess');
		Zend_Session::setSaveHandler(self::$mongoSession);
		if (self::isHttps()){
			Zend_Session::setOptions(array(
				'cookie_secure' => true,
			));
		}

		//ログイン状態を調べて、もしログインしていないなら長期保存セッションも調べる
		self::$sess = new Zend_Session_Namespace();
		if (!isset(self::$sess->user) || !is_array(self::$sess->user))
		{
			$_id = self::$mongoSession->readLong();
			if ($_id){
				self::$sess->user = User::get(new MongoId($_id));
				Worldline::rememberMe(60*60*24*30);
			}
		}
	}

	static public function rememberMe($seconds){
		Zend_Session::rememberMe($seconds);
		self::$mongoSession->writeLong((string)self::$sess->user['_id'], $seconds);
	}
}
Worldline::init();
