<?php
require_once 'Zend/Session/SaveHandler/Interface.php';
class MongoSession implements Zend_Session_SaveHandler_Interface {
	private $server;
	private $db;
	private $collection;
	private $mongo = null;
	private $col = null;
	private $long_col = null;

	public function __construct($db = 'php', $collection = 'sessions', $server = 'mongodb://localhost:27017'){
		$this->db = $db;
		$this->collection = $collection;
		$this->server = $server;
	}

	public function open($save_path, $session_name){
		if (!is_null($this->mongo)) {
			return;
		}

		$this->mongo = new Mongo($this->server);
		if ($this->mongo->connected) {
			$db = $this->mongo->selectDB($this->db);
			$this->col = $db->selectCollection($this->collection);
			$this->long_col = $db->selectCollection($this->collection . '_long');
		}

		return $this->mongo->connected;
	}

	public function close(){
		return true;
	}

	public function read($id){
		if (!$this->col) return '';
		$item = $this->col->findOne(
			array('_id' => $id), array('data' => true)
		);
		return (string) $item['data'];
	}

	public function write($id, $sess_data){
		if (!$this->col) return false;
		$this->col->save(array(
			'_id' => $id,
			'data' => $sess_data,
			'ts' => new MongoDate()
		), array('safe' => true));
	}

	public function readLong(){
		if (!$this->long_col) return '';
		$item = $this->long_col->findOne(
			array('_id' => session_id()), array('data' => true)
		);
		if ($item === null) return null;
		return unserialize((string) $item['data']);
	}

	public function writeLong($sess_data, $lifetime = 2592000/* 60*60*24*30 */){
		if (!$this->long_col) return false;
		$this->long_col->save(array(
			'_id' => session_id(),
			'data' => serialize($sess_data),
			'ts' => new MongoDate(time() + $lifetime)
		), array('safe' => true));
		return true;
	}

	public function destroy($id){
		if (!$this->col) return false;
		$this->col->remove(
			array('_id' => $id), array('justOne' => true)
		);
		$this->long_col->remove(
			array('_id' => $id), array('justOne' => true)
		);
		return true;
	}

	public function gc($maxlifetime){
		if (!$this->col) return false;
		$this->col->remove(
			array(
				'ts' => array('$lt' => new MongoDate(time() - $maxlifetime))
			)
		);
		$this->long_col->remove(
			array(
				'ts' => array('$lt' => new MongoDate(time()))
			)
		);
		return true;
	}
}
