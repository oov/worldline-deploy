<?php
require_once 'Worldline.php';
require_once 'Mustache.php';

$sess = Worldline::getDefaultSession();

function jstime(){
	return floor(microtime(true)*1000);
}

function mbtrim($s){
	return mb_ereg_replace('(^[\0\s]+|[\0\s]+$)', '', $s);
}

function getParam($name, $default = ''){
	if (isset($_POST[$name])) return $_POST[$name];
	return isset($_GET[$name]) ? $_GET[$name] : $default;
}

function convertArrayToJSON(&$v){
	if (is_array($v)){
		foreach($v as &$a){
			convertArrayToJSON($a);
		}
	} else if ($v instanceof MongoId || $v instanceof MongoInt64) {
		$v = (string)$v;
	} else if ($v instanceof MongoDate) {
		$v = $v->sec * 1000 + $v->usec / 1000;
	}
}

function jstimeToISO8601($t){
	return gmdate("Y-m-d\\TH:i:s", $t / 1000) . sprintf(".%03dZ", $t % 1000);
}

function createSquareThumbImage($source, $size = 128, $enlargement = false){
	//保存先になる画像を作成
	$thumb = imagecreatetruecolor($size, $size);
	try{
		//白で塗りつぶしておく
		imagefilledrectangle($thumb, 0, 0, $size, $size, 0xffffff);

		//コピー元になる画像を読み込む
		$imagesize = getimagesize($source);
		if (!$imagesize){
			throw new Exception('Could not read image information');
		}

		list($width, $height, $type) = $imagesize;
		if (!$width || !$height){
			throw new Exception('Could not get image width/height');
		}

		switch($type){
		case IMAGETYPE_GIF:  $img = imagecreatefromgif ($source); break;
		case IMAGETYPE_JPEG: $img = imagecreatefromjpeg($source); break;
		case IMAGETYPE_PNG:  $img = imagecreatefrompng ($source); break;
		}
		if (!$img){
			throw new Exception('Could not read image');
		}
		try{
			//リサイズ後の大きさを算出する
			if (($width > $size && $height > $size) || $enlargement){
				//小さい方を出力先に合わせてはみ出した分は切り落とす
				if ($width < $height){
					$thumb_width  = $size;
					$thumb_height = ($size/$width)*$height;
				} else {
					$thumb_height = $size;
					$thumb_width  = ($size/$height)*$width;
				}
			} else {
				$thumb_width  = $width;
				$thumb_height = $height;
			}
			if (!imagecopyresampled(
				$thumb, $img,
				($size - $thumb_width) / 2, ($size - $thumb_height) / 2,
				0, 0,
				$thumb_width, $thumb_height,
				$width, $height
			)){
				throw new Exception('Could not resize image');
			}
		}catch(Exception $e){
			imagedestroy($img);
			throw $e;
		}
		imagedestroy($img);
	}catch(Exception $e){
		imagedestroy($thumb);
		throw $e;
	}
	return $thumb;
}

function showError($msg, $title, $status = "404 Not Found"){
	require_once 'error.php';
	outputErrorPage($status, $msg, $title);
}

function echoHTMLJSON($json){
	header("Access-Control-Allow-Headers: *");
	header("Access-Control-Allow-Methods:POST");
	header("Access-Control-Allow-Origin: " . Worldline::getBaseURL());
	header("X-Content-Security-Policy: allow 'self' " . Worldline::getDomain());
	header("X-Content-Type-Options: nosniff");
	header("X-Frame-Options: SAMEORIGIN");
	header("X-Xss-Protection:1; mode=block");
	header("Content-Type: text/html; charset=UTF-8");
	echo '<!DOCTYPE html><html lang="ja"><meta charset="UTF-8">';
	echo htmlspecialchars(json_encode($json), ENT_QUOTES, 'UTF-8');
	echo '</html>';
	exit;
}

function echoJSON($json){
	header("Access-Control-Allow-Headers: *");
	header("Access-Control-Allow-Methods:POST");
	header("Access-Control-Allow-Origin: " . Worldline::getBaseURL());
	header("X-Content-Security-Policy: allow 'self' " . Worldline::getDomain());
	header("X-Content-Type-Options: nosniff");
	header("X-Frame-Options: SAMEORIGIN");
	header("X-Xss-Protection:1; mode=block");
	header("Content-Type: application/json; charset=UTF-8");
	echo json_encode($json);
	exit;
}

class User {
	static private $userCache = array();
	static public function get(MongoId $_id, $useCache = true){
		$strId = (string)$_id;
		if (!$useCache || !isset(self::$userCache[$strId])){
			$doc = Worldline::getUsersCollection()->findOne(array('_id' => $_id));
			self::$userCache[$strId] = $doc;
		}
		return self::$userCache[$strId];
	}
}

class HeadTemplate extends Mustache {
	public $user;
	public $title;
	public $css;
	public $js;

	public function __construct($template = self::TEMPLATE, $view = null, $partials = null, array $options = null) {
		parent::__construct($template, $view, $partials, $options);
	}

	public function nodeTemplate(){
		return NodeTemplate::TEMPLATE . NodeTemplate::TEMPLATE_FOOT;
	}

	public function userSettingDialogTemplate(){
		return self::USER_SETTING_DIALOG_TEMPLATE;
	}

	public function postTemplate(){
		return self::POST_TEMPLATE;
	}

	public function removedTemplate(){
		return self::REMOVED_TEMPLATE;
	}

	public function userJSON(){
		$user = $this->user;
		convertArrayToJSON($user);
		if (is_array($user)){
			$user['token'] = WorldlineSecure::getToken($user);
		}
		return json_encode($user);
	}

	const POST_TEMPLATE =<<<EOT
<form action="/" method="post" enctype="multipart/form-data" target="post-target" class="well">
	<input type="hidden" name="a" value="post">
	<input type="hidden" name="tk" value="{{token}}">
	<input type="hidden" name="parent" value="{{id}}">
	<textarea class="row-fluid" type="text" name="title" autocomplete="off"></textarea>
	<div class="row-fluid">
		<div class="span2">
			<input class="span12 btn" type="submit" value="送信">
		</div>
		<div class="span2">
			<label class="checkbox inline"><input type="checkbox" name="ninja" value="1"> 忍者</label>
		</div>
		<div class="span8">
			<input type="file" name="file">
		</div>
	</div>
</form>
<iframe id="post-target" name="post-target" style="position:absolute;left:0;top:0;width:1px;height:1px;opacity:0;filter:alpha(opacity=0);"></iframe>
EOT;
	const USER_SETTING_DIALOG_TEMPLATE =<<<EOT
	<div class="modal-header">
		<a class="close" data-dismiss="modal">×</a>
		<h3>アカウント設定</h3>
	</div>
	<div class="modal-body">
		<form action="/" method="post" enctype="multipart/form-data">
			<input type="hidden" name="a" value="prof">
			<input type="hidden" name="tk" value="{{token}}">
			<dl>
				<dt>プロフィール画像</dt>
				<dd><img src="{{#user.avatar}}/f{{user._id}}.jpg?{{user.avatar}}{{/user.avatar}}{{^user.avatar}}/img/avatar.png{{/user.avatar}}" width="128" height="128" alt=""></dd>
				<dd><input type="file" name="file"></dd>
				<dt>名前</dt>
				<dd><input class="span3" type="text" name="name" autocomplete="off" value="{{user.name}}"></dd>
				<dt>キャッチコピー</dt>
				<dd><input class="span5" type="text" name="catch" autocomplete="off" value="{{user.catch}}"></dd>
			</dl>
		</form>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn btn-primary">保存</a>
		<a href="#" class="btn" data-dismiss="modal">閉じる</a>
	</div>
EOT;
	const TEMPLATE =<<<EOT
<!DOCTYPE html>
<html lang="ja">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="/favicon.ico" type="image/vnd.microsoft.icon">
<link rel="icon" href="/favicon.ico" type="image/vnd.microsoft.icon">
<title>{{title}}</title>
<link href="/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="/css/worldline.css?20120516a" rel="stylesheet">
{{&css}}
<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<!--[if IE]><script src="/js/jquery.ie.cors.js"></script><![endif]-->
<script src="/js/jquery.timeago.js"></script>
<script src="/js/mustache.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/worldline.js?20120731"></script>
{{&js}}
<script>var user = {{&userJSON}};</script>
<script type="text/template" id="post-template">{{&postTemplate}}</script>
<script type="text/template" id="node-template">{{&nodeTemplate}}</script>
<script type="text/template" id="user-setting-dialog">{{&userSettingDialogTemplate}}</script>
<div class="container">
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
			<a class="brand" href="/">
				<img src="/img/worldline.png" width="80" height="80">
				WORLDLINE
			</a>
			<ul class="nav pull-right">
			<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						{{#user}}
							<img class="avatar-mini" src="{{#user.avatar}}/f{{user._id}}.thumb.jpg?{{user.avatar}}{{/user.avatar}}{{^user.avatar}}/img/avatar_thumb.png{{/user.avatar}}" width="24" height="24" alt="">
							{{user.name}}
						{{/user}}
						{{^user}}
							ゲスト
						{{/user}}
						さん <b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
						{{#user}}
							<li><a href="/u{{user._id}}">自分のプロフィールを確認</a></li>
							<li><a class="change-account-setting" href="javascript:void(0)">アカウント設定</a></li>
							<li class="divider"></li>
							<li><a href="/?a=logout">ログアウト</a></li>
						{{/user}}
						{{^user}}
							<li><a href="/?a=login&amp;via=heello">Heello アカウントでログイン</a></li>
							<li><a href="/?a=login&amp;via=twitter">Twitter アカウントでログイン</a></li>
							<li><a href="/?a=login&amp;via=google">Google アカウントでログイン</a></li>
							<li><a href="/?a=login&amp;via=instagram">Instagram アカウントでログイン</a></li>
						{{/user}}
					</ul>
				</li>
			</ul>
    </div>
  </div>
</div>
<div style="margin-bottom:58px" class="visible-desktop"></div>
EOT;
}

class NodeTemplate extends Mustache {
	public $current_user;
	public $node;
	public $omit;

	public function __construct($template = self::TEMPLATE, $view = null, $partials = null, array $options = null) {
		parent::__construct($template, $view, $partials, $options);
	}

	public function node_title_decorated(){
		return nl2br(htmlspecialchars($this->node['title'], ENT_QUOTES, 'UTF-8'));
	}

	public function node_adddate_iso8601(){
		return jstimeToISO8601($this->node['adddate']);
	}

	public function node_moddate_iso8601(){
		return jstimeToISO8601($this->node['moddate']);
	}

	public function current_user_post(){
		return $this->node['user']['_id'] && $this->current_user['_id'] == $this->node['user']['_id'];
	}

	public function render($template = null, $view = null, $partials = null) {
		if (is_array($this->node) && isset($this->node['user']) && is_array($this->node['user'])){
			$user = User::get($this->node['user']['_id']);
			if (isset($user['avatar'])){
				$this->node['user']['avatar'] = $user['avatar'];
			}
		}
		return parent::render($template, $view, $partials);
	}
	const ROOT_TEMPLATE =<<<EOT
<li id="n{{node._id}}" class="node node-full node-root{{#node.del}} node-removed{{/node.del}}" data-id="{{node._id}}"{{#node}} data-moddate="{{node.moddate}}" data-adddate="{{node.adddate}}"{{/node}}>
	<div class="node-container">
		<div class="body">
			<div class="header root-header">
				{{#node}}
					<a class="btn" href="/{{#node.parent}}?world={{node.parent}}{{/node.parent}}"><i class="icon-globe"></i> 上のワールドへ</a>
				{{/node}}
			</div>
			{{#node.del}}
				<div class="info">
					<span class="name name-anonymous">
						<img class="avatar-mini" src="/img/avatar_thumb.png" width="24" height="24" alt="">
						誰かさん
					</span>
					{{#node.moddate}}<span class="time" title="{{node_moddate_iso8601}}">{{node_moddate_iso8601}}</span>{{/node.moddate}}
				</div>
				<div class="title">（削除）</div>
			{{/node.del}}
			{{^node.del}}
				<div class="info">
					{{#node.user.name}}
						{{#node.ninja}}
							<span class="name name-anonymous">
								<img class="avatar-mini" src="/img/avatar_thumb.png" width="24" height="24" alt="">
								誰かさん
							</span>
						{{/node.ninja}}
						{{^node.ninja}}
							<a class="name" href="/u{{node.user._id}}">
								<img class="avatar-mini" src="{{#node.user.avatar}}f{{node.user._id}}.thumb.jpg?{{node.user.avatar}}{{/node.user.avatar}}{{^node.user.avatar}}/img/avatar_thumb.png{{/node.user.avatar}}" width="24" height="24" alt="">
								{{node.user.name}}
							</a>
						{{/node.ninja}}
					{{/node.user.name}}
					{{#node.moddate}}<span class="time" title="{{node_moddate_iso8601}}">{{node_moddate_iso8601}}</span>{{/node.moddate}}
					{{#current_user_post}}
						{{^node.del}}
							<a class="btn btn-danger btn-mini remove-post" href="./?remove={{node._id}}"><i class="icon-white icon-remove"></i></a>
						{{/node.del}}
					{{/current_user_post}}
				</div>
				{{#node.file}}
					<div class="file">
						<a target="_blank" href="/f{{node.file}}{{node.file_ext}}">
							<img src="/f{{node.file}}.thumb{{node.file_ext}}" width="64" height="64" alt="">
						</a>
					</div>
				{{/node.file}}
				<div class="title">{{^node.title}}グローバル{{/node.title}}{{&node_title_decorated}}</div>
			{{/node.del}}
		</div>
		<a class="btn write-res" href="./?world={{node._id}}"><i class="icon-pencil"></i> このワールドに新規投稿</a>
	</div>
	<div class="interface"></div>
	<ul>
EOT;
	const TEMPLATE =<<<EOT
<li id="n{{node._id}}" class="node node-{{#omit}}omit{{/omit}}{{^omit}}full{{/omit}}{{#node.del}} node-removed{{/node.del}}" data-id="{{node._id}}" data-moddate="{{node.moddate}}" data-adddate="{{node.adddate}}">
	<div class="node-container">
		<div class="body">
			{{#node.del}}
				<div class="info">
					<span class="name name-anonymous">
						<img class="avatar-mini" src="/img/avatar_thumb.png" width="24" height="24" alt="">
						誰かさん
					</span>
					{{#omit}}<span class="time" title="{{node_moddate_iso8601}}">{{node_moddate_iso8601}}</span>{{/omit}}
					{{^omit}}<span class="time" title="{{node_adddate_iso8601}}">{{node_adddate_iso8601}}</span>{{/omit}}
					{{^omit}}
						<a class="btn btn-mini write-res" href="./?world={{node._id}}"><i class="icon-pencil"></i></a>
					{{/omit}}
				</div>
				<div class="title">（削除）</div>
			{{/node.del}}
			{{^node.del}}
				<div class="info">
					{{#node.ninja}}
						<span class="name name-anonymous">
							<img class="avatar-mini" src="/img/avatar_thumb.png" width="24" height="24" alt="">
							誰かさん
						</span>
					{{/node.ninja}}
					{{^node.ninja}}
						<a class="name" href="/u{{node.user._id}}">
							<img class="avatar-mini" src="{{#node.user.avatar}}f{{node.user._id}}.thumb.jpg?{{node.user.avatar}}{{/node.user.avatar}}{{^node.user.avatar}}/img/avatar_thumb.png{{/node.user.avatar}}" width="24" height="24" alt="">
							{{node.user.name}}
						</a>
					{{/node.ninja}}
					{{#omit}}<span class="time" title="{{node_moddate_iso8601}}">{{node_moddate_iso8601}}</span>{{/omit}}
					{{^omit}}<span class="time" title="{{node_adddate_iso8601}}">{{node_adddate_iso8601}}</span>{{/omit}}
					{{^omit}}
						<a class="btn btn-mini write-res" href="./?world={{node._id}}"><i class="icon-pencil"></i></a>
					{{/omit}}
					{{#current_user_post}}
						<a class="btn btn-danger btn-mini remove-post" href="./?remove={{node._id}}"><i class="icon-white icon-remove"></i></a>
					{{/current_user_post}}
				</div>
				{{#node.file}}
					<div class="file">
						<a target="_blank" href="/f{{node.file}}{{node.file_ext}}">
							<img src="/f{{node.file}}.thumb{{node.file_ext}}" width="64" height="64" alt="">
						</a>
					</div>
				{{/node.file}}
				<div class="title">{{&node_title_decorated}}</div>
			{{/node.del}}
		</div>
		{{#omit}}
			<div class="footer">
				<a class="btn btn-primary" href="./?world={{node._id}}"><i class="icon-white icon-globe"></i> OVER THE WORLDLINE</a>
			</div>
		{{/omit}}
	</div>
	<ul>
EOT;
	const ROOT_TEMPLATE_FOOT =<<<EOT
	</ul>
</li>
EOT;
	const TEMPLATE_FOOT =<<<EOT
	</ul>
	<div class="interface"></div>
</li>
EOT;
}

class FootTemplate extends Mustache {
	public function __construct($template = self::TEMPLATE, $view = null, $partials = null, array $options = null) {
		parent::__construct($template, $view, $partials, $options);
	}

const TEMPLATE =<<<EOT
<hr>
WORLDLINE - <a href="https://chrome.google.com/webstore/detail/gpnpaiefiikjhmcmkdfljboapnkiiimi" target="_blank">ChromeExtension</a> - <a href="https://bitbucket.org/oov/worldline-deploy" target="_blank">SourceCode</a>
</div>
</html>
EOT;
}
