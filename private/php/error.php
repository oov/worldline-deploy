<?php
class ErrorTemplate extends Mustache {
	public $title;
	public $msg;

	public function __construct($template = self::TEMPLATE, $view = null, $partials = null, array $options = null) {
		parent::__construct($template, $view, $partials, $options);
	}

	const TEMPLATE =<<<EOT
<div class="alert alert-error">
	<h1>{{title}}</h1>
	<p>{{msg}}</p>
</div>
<p>
	<a class="btn btn-primary btn-large" href="/">グローバルに戻る</a>
</p>
EOT;
}

function outputErrorPage($status, $msg, $title){
	header("Status: " . $status);

	$sess = new Zend_Session_Namespace();

	$head = new HeadTemplate();
	$head->user = $sess->user;
	$head->title = $title . ' - WORLDLINE';
	echo $head->render();
	
	$err = new ErrorTemplate();
	$err->title = $title;
	$err->msg = $msg;
	echo $err->render();

	$head = new FootTemplate();
	$head->user = $sess->user;
	$head->title = $title . ' - WORLDLINE';
	echo $head->render();
	exit();
}
