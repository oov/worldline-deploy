<?php
set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__FILE__));
require_once 'common.php';

$action = getParam('a');
if ($action){
	switch($action){
	case "user":
		require_once 'user.php';
		exit;
	case "remove":
		$nodeId = getParam('id');
		if ($nodeId){
			$nodeId = new MongoId($nodeId);
		} else {
			echoJSON(array("error" => '不正な呼び出しです'));
		}

		if (!WorldlineSecure::verifyToken($sess->user, getParam('tk'))){
			echoJSON(array("error" => 'トークンIDが不正なため処理を続行できません'));
		}

		$tree = Worldline::getTreeCollection();
		$r = $tree->update(
			array(
				"_id" => $nodeId,
				"user._id" => $sess->user['_id'],
				"del" => array('$exists' => false),
			),
	 		array(
				'$set' => array(
					"del" => jstime(),
				),
			),
			array("safe" => true)
		);

		if (!$r["updatedExisting"]){
			echoJSON(array("error" => '記事の削除に失敗しました'));
		}
		echoJSON(true);
	case "post":
		$parent = getParam('parent');
		if ($parent){
			$parent = new MongoId($parent);
		} else {
			$parent = null;
		}
		$title = mbtrim(getParam('title'));
		$worldId = null;

		if (!WorldlineSecure::verifyToken($sess->user, getParam('tk'))){
			echoHTMLJSON(array("error" => 'トークンIDが不正なため処理を続行できません'));
		}

		if (!$sess->user){
			echoHTMLJSON(array("error" => '投稿するためにはログインする必要があります'));
		}

		if (!$title) echoHTMLJSON(array("error" => '本文が空欄のままでは投稿できません'));

		//TODO: エラーチェックが甘いのと処理がゴチャゴチャしてきたから整理したいのと、エラーの返し方がまずいので直したい
		//ファイル
		//PHPのコード上では5MB/一辺1000px以内
		if (isset($_FILES["file"]) && $_FILES["file"]["tmp_name"]){
			$file = $_FILES["file"];
			if ($file['size'] > 1024 * 1024 * 8) echoHTMLJSON(array("error" => 'アップロードしようとしている画像が大きすぎます'));

			$info = getimagesize($file['tmp_name']);
			if (!$info) echoHTMLJSON(array("error" => 'アップロードできるのは GIF / JPEG / PNG 形式の画像のみです'));

			list($width, $height, $type) = $info;

			if ($width > 5000 || $height > 5000) echoHTMLJSON(array("error" => 'アップロードしようとしている画像が大きすぎます'));
			switch($type){
			case IMAGETYPE_GIF: $ext = ".gif"; break;
			case IMAGETYPE_JPEG: $ext = ".jpg"; break;
			case IMAGETYPE_PNG: $ext = ".png"; break;
			default: echoHTMLJSON(array("error" => 'アップロードできるのは GIF / JPEG / PNG 形式の画像のみです'));
			}

			$hash = sha1_file($file['tmp_name']);
			$thumb_filename = tempnam(sys_get_temp_dir(), 'worldline');
			try{
				$thumb = createSquareThumbImage($file['tmp_name'], 128, false);
				try{
					if (!imagejpeg($thumb, $thumb_filename, 85)){
						throw new Exception('Could not save thumbnail image');
					}
				}catch(Exception $e){
					imagedestroy($thumb);
					throw $e;
				}
				imagedestroy($thumb);

				//サムネイルの作成が終わったので画像をGridFS上に保存する
				$gfs = Worldline::getFileGridFS();
				$gfs->storeUpload("file", array("filename" => "{$hash}{$ext}"));
				$gfs->storeFile($thumb_filename, array("filename" => "{$hash}.thumb{$ext}"));
			}catch(Exception $e){
				@unlink($thumb_filename);
				echoHTMLJSON(array("error" => '画像データの処理中にエラーが発生しました: ' . $e->getMessage()));
			}
			unlink($thumb_filename);
		}

		if ($parent && !incrementParents($parent, 1, $_)){
			echoHTMLJSON(array("error" => "投稿できませんでした"));
		}

		$doc = array(
			"parent" => $parent,
			"title" => $title,
			"adddate" => new MongoInt64(jstime()),
			"moddate" => new MongoInt64(jstime()),
			"nodes" => 0,
			"user" => array(
				"_id" => new MongoId($sess->user['_id']),
				"name" => $sess->user['name'],
			),
		);
		if (!!getParam('ninja')){
			$doc["ninja"] = 1;
		}
		if (isset($hash)){
			$doc["file"] = $hash;
			$doc["file_ext"] = $ext;
		}
		Worldline::getTreeCollection()->insert($doc);

		//ユーザーデータのavatar部分は自分の情報で差し替えておく
		if (isset($sess->user['avatar']) && $sess->user['avatar']){
			$doc['user']['avatar'] = $sess->user['avatar'];
		} else {
			unset($doc['user']['avatar']);
		}
		convertArrayToJSON($doc);
		echoHTMLJSON($doc);
		break;
	case "prof":
		if (!$sess->user){
			showError('不正な呼び出しです', 'Error', '200 OK');
		}

		if (!WorldlineSecure::verifyToken($sess->user, getParam('tk'))){
			showError('トークンIDが不正なため処理を続行できません', 'Error', '200 OK');
		}

		$name = mbtrim(getParam('name'));
		if (!$name){
			showError('名前は空欄にできません', 'Name required', '200 OK');
		}

		$update = array(
			'$set' => array(
				"moddate" => jstime(),
				"name" => $name,
			),
			'$unset' => array(
			),
		);

		$catch = mbtrim(getParam('catch'));
		if ($catch){
			$update['$set']['catch'] = $catch;
		} else {
			$update['$unset']['catch'] = 1;
		}

		if (isset($_FILES["file"]) && $_FILES["file"]["tmp_name"]){
			$file = $_FILES["file"];
			if ($file['size'] > 1024 * 1024 * 8){
				showError('アップロードしようとしている画像が大きすぎます', 'Image is too big', '200 OK');
			}

			$info = getimagesize($file['tmp_name']);
			if (!$info){
				showError('アップロードされたファイルが読み込めません', 'Could not read image', '200 OK');
			}

			list($width, $height, $type) = $info;

			if ($width > 5000 || $height > 5000){
				showError('アップロードしようとしている画像が大きすぎます', 'Image is too big', '200 OK');
			}
			switch($type){
				case IMAGETYPE_GIF: case IMAGETYPE_JPEG: case IMAGETYPE_PNG: break;
				default: showError('アップロードできるのは GIF / JPEG / PNG 形式の画像のみです', 'Unsupported image format', '200 OK');
			}

			$hash = $sess->user['_id'];
			$filename = tempnam(sys_get_temp_dir(), 'worldline');
			$thumb_filename = tempnam(sys_get_temp_dir(), 'worldline');
			try{
				$img = createSquareThumbImage($file['tmp_name'], 128, false); try{
					if (!imagejpeg($img, $filename, 85)){
						throw new Exception('Could not save thumbnail image');
					}
				}catch(Exception $e){
					imagedestroy($img);
					throw $e;
				}
				imagedestroy($img);

				$img = createSquareThumbImage($file['tmp_name'], 48, false);
				try{
					if (!imagejpeg($img, $thumb_filename, 85)){
						throw new Exception('Could not save thumbnail image');
					}
				}catch(Exception $e){
					imagedestroy($img);
					throw $e;
				}
				imagedestroy($img);

				//画像を2枚作ったので gridFS に保存する
				$gfs = Worldline::getFileGridFS();
				$gfs->storeFile($filename, array("filename" => "$hash.jpg"));
				$gfs->storeFile($thumb_filename, array("filename" => "$hash.thumb.jpg"));
			}catch(Exception $e){
				@unlink($filename);
				@unlink($thumb_filename);
				showError('画像データの処理中にエラーが発生しました: ' . $e->getMessage(), 'Could not save image', '200 OK');
			}
			$update['$set']['avatar'] = time();
			@unlink($filename);
			@unlink($thumb_filename);
		} else {
			//TODO: 画像の削除は別のインターフェイスで行うようにしないと駄目だった
			//$update['$unset']['avatar'] = 1;
		}

		if (!count($update['$set'])) unset($update['$set']);
		if (!count($update['$unset'])) unset($update['$unset']);

		$r = Worldline::getUsersCollection()->update(
			array(
				"_id" => $sess->user['_id'],
			),
			$update,
			array("safe" => true)
		);
		if (!$r["updatedExisting"]){
			showError('プロフィールの更新に失敗しました', 'Could not update profile', '200 OK');
		}
		$sess->user = User::get($sess->user['_id'], false);
		header("Location: " . Worldline::getBaseURL());
		break;
	case "login":
		$via = getParam('via', $sess->via);
		switch($via){
			case 'heello':
				$authorizer = Worldline::getHeelloAuthorizer();
				break;
			case 'twitter':
				$authorizer = Worldline::getTwitterAuthorizer();
				break;
			case 'google':
				$authorizer = Worldline::getGoogleAuthorizer();
				break;
			case 'instagram':
				$authorizer = Worldline::getInstagramAuthorizer();
				break;
			default:
				showError('不正なリクエストです', 'Invalid request', '200 OK');
		}
		$sess->via = $via;
		try{
			$token = $authorizer->auth();
		}catch(Exception $e){
			showError('認証に失敗しました: ' . $e->getMessage(), 'Authorization failure', '200 OK');
		}

		//認証に成功した
		unset($sess->via);
		$from = WorldlineSecure::calcUserHash("$via-$token[id]");
		$doc = array(
			"from" => $from,
			"name" => $token['name'],
			"adddate" => new MongoInt64(jstime()),
			"moddate" => new MongoInt64(jstime()),
		);
		try{
			Worldline::getUsersCollection()->insert($doc, array("safe" => true));
			$sess->firstLogin = true;
			$sess->setExpirationHops(1, 'firstLogin');
		}catch(MongoCursorException $e){
			//既に認証済みだったぽい
			$doc = Worldline::getUsersCollection()->findOne(array("from" => $from));
		}
		$sess->user = $doc;
		Worldline::rememberMe(60*60*24*30);
		header("Location: " . Worldline::getBaseURL());
		break;
	case "logout":
		//TODO: 別に保存しておいた長期間ログイン維持用のデータも始末する
		Zend_Session::destroy(true);
		header("Location: " . Worldline::getBaseURL());
		break;
	}
	exit;
}

//ワールドに出会うまで親を遡ってインクリメントしていく
function incrementParents($nodeId, $value, &$depth){
	$tree = Worldline::getTreeCollection();
	$doc = $tree->findOne(array("_id" => $nodeId));
	$r = $tree->update(
		array(
			"_id" => $nodeId,
		),
 		array(
			'$set' => array(
				"moddate" => jstime(),
			),
			'$inc' => array(
				"nodes" => $value,
			),
		),
		array("safe" => true)
	);
	//ワールドラインの上限まで親を辿って更新日時を変更する
	if ($doc['parent'] && ++$depth < Worldline::WORLDLINE){
		return $r["updatedExisting"] && incrementParents($doc['parent'], $value, $depth);
	} else {
		$worldId = null;
		return $r["updatedExisting"];
	}
}

//現在のワールドを取得する
$world = getParam('world');
if (!empty($world)){
	$world = new MongoId($_GET["world"]);
	$worldNode = Worldline::getTreeCollection()->findOne(array("_id" => $world/*, "del" => array('$exists' => false)*/));
	if (!$worldNode){
		showError('指定されたIDのワールドは見つかりませんでした。', 'World not found');
	}
} else {
	$world = null;
}

header("Content-Type: text/html; charset=UTF-8");

if (Worldline::isXmlHttpRequest()){
	//XmlHttpRequestの場合はツリーの本文部分だけを返して終わる
	outputTree($sess->user, $world, Worldline::LIMIT, intval(getParam('o')));
	exit;
}

//再帰でルートだけ拾ってくるのだるいから手抜き…
class OldestModdateStore {
	static public $d = 0;
}

$template = new HeadTemplate();
$template->title = "WORLDLINE";
$template->user = $sess->user;
echo $template->render();
echo '<script>var logMode='.json_encode(!!intval(getParam('o'))).';</script>';

//名前変えずに投稿する人が多かったので
//初回ログイン直後にだけ現れるページ
if (isset($sess->firstLogin) && $sess->firstLogin){
	$m = new Mustache();
	echo $m->render(<<<EOT
<div class="alert alert-success">
	<a class="close" data-dismiss="alert" href="#">×</a>
	<h1>WORLDLINE へようこそ</h1>
	<p>
		ようこそ {{user.name}} さん、まずはプロフィールを設定しましょう。<br>
		この設定は右上のメニューからいつでも変更可能です。
	</p>
	<p>
		Google Chrome を使用している場合は
		<a href="https://chrome.google.com/webstore/detail/gpnpaiefiikjhmcmkdfljboapnkiiimi" target="_blank">Chrome 拡張</a>
		をインストールしておくと手軽に新着をチェックできます。
	</p>
	<a class="btn btn-primary btn-large change-account-setting">アカウント設定</a>
</div>
EOT
	, array('user'=>$sess->user));
}

echo '<ul class="tree">';
$root = new NodeTemplate(NodeTemplate::ROOT_TEMPLATE);
if (isset($worldNode)){
	$root->node = $worldNode;
}
$root->current_user = $sess->user;
echo $root->render();

//ワールドツリー
outputTree($sess->user, $world, Worldline::LIMIT, intval(getParam('o')));
echo NodeTemplate::ROOT_TEMPLATE_FOOT;
echo '</ul>';

//１件でも出力されていたらより古いものへのリクエストをできるようにリンクを置いておく
if (OldestModdateStore::$d){
	$m = new Mustache();
	echo $m->render(<<<EOT
<ul class="pager">
	<li><a class="more" href="/?{{#node}}world={{node._id}}&amp;{{/node}}o={{moddate}}" data-world="{{node._id}}" data-o="{{moddate}}">MORE</a></li>
</ul>
EOT
	, array(
		'moddate' => OldestModdateStore::$d,
		'node' => isset($worldNode) ? $worldNode : null,
	));
}

//再帰でツリーを構築する
function outputTree($current_user, $parent, $limit = 0, $skip = 0){
	$cursor = buildTreeQuery($parent, $limit, $skip);
	foreach($cursor as $node){
		if ($limit){
			OldestModdateStore::$d = $node['moddate'];
		}
		if (isset($node['del']) && !$node['nodes']){
			//TODO: 子がひとつもついておらず、削除済みのノードがあった場合はスルー
			//      通知サーバ側でも削除を認識するようにしないと上手く動かない
			//continue;
		}
		$omit = $node['nodes'] >= Worldline::WORLDLINE;
		echo renderNodeBody($current_user, $node, $omit);
		if (!$omit) outputTree($current_user, $node['_id']);
		echo NodeTemplate::TEMPLATE_FOOT;
	}
}

function renderNodeBody($current_user, $node, $omit){
	$template = new NodeTemplate();
	$template->node = $node;
	$template->omit = $omit;
	$template->current_user = $current_user;
	$template->appendix = isset($node['appendix']) ? $node['appendix'] : '';
	return $template->render();
}

//データ取得のためのクエリを組み立てる処理
function buildTreeQuery($parent, $limit, $skip){
	$f = array("parent" => $parent);
	if ($skip){
		$f["moddate"] = array('$lt' => new MongoInt64($skip));
	}
	$cursor = Worldline::getTreeCollection()->find($f)->sort($limit ? array("moddate" => -1) : array("adddate" => 1));
	if ($limit) $cursor->limit($limit);
	return $cursor;
}

$foot = new FootTemplate();
echo $foot->render();
