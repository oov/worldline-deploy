package main

import(
	"log"
	"container/ring"
	"net/http"
	"code.google.com/p/go.net/websocket"
	"launchpad.net/mgo/bson"
	"strconv"
)

type ReceiveData struct {
	Id []string `json:"id"`
	Origin int64 `json:"o"`
}

type AppendRequest struct {
	Ws *websocket.Conn
	Id []string
	Origin int64
}

type RemoveRequest struct {
	Ws *websocket.Conn
}

type BroadcastRequest struct {
	NodeData NodeDocument
}

type GetLatestRequest struct {
	Origin int64
	Result chan<- *LatestLog
}

type LatestLog struct {
	Json []byte
	Parent bson.ObjectId
	AddDate int64
}

var (
	adddateRequest = make(chan interface{}, 100)
	adddateLatestPosts = ring.New(100)
)

func watcher(ws *websocket.Conn) {
	defer func(){
		adddateRequest <- &RemoveRequest{
			Ws: ws,
		}
	}()

	var err error
	var r = ReceiveData{
		Id: []string{},
	}

	for {
		err = websocket.JSON.Receive(ws, &r)
		if err != nil {
			break
		}
		adddateRequest <- &AppendRequest{
			Ws: ws,
			Id: r.Id,
			Origin: r.Origin,
		}
	}
}

func ajax(w http.ResponseWriter, req *http.Request){
	origin, err := strconv.ParseInt(req.FormValue("o"), 10, 64)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	r := make(chan *LatestLog)
	adddateRequest <- &GetLatestRequest{
		Origin: origin,
		Result: r,
	}

	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("X-XSS-Protection", "1; mode=block")
	w.Header().Set("X-Frame-Options", "SAMEORIGIN")
	w.Header().Set("X-Content-Security-Policy", "allow 'self' " + *domain)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	var allowOrigin string
	if *certFile == "" || *keyFile == "" {
		allowOrigin = "http"
	} else {
		allowOrigin = "https"
	}
	w.Header().Set("Access-Control-Allow-Origin", allowOrigin + "://" + *domain)
	w.Header().Set("Access-Control-Allow-Methods", "GET")
	w.Header().Set("Access-Control-Allow-Headers", "*")

	first := true
	w.Write([]byte("["))
	for {
		log := <-r;
		if len(log.Json) == 0 {
			break
		}
		if first {
			first = false;
		} else {
			w.Write([]byte(","))
		}
		w.Write(log.Json)
	}
	w.Write([]byte("]"))
}

func manageAddDateWatchers() {
	watchers := map[string]map[*websocket.Conn]None{}

	for {
		switch r := (<-adddateRequest).(type) {
		case *AppendRequest:
			for _, id := range r.Id {
				wl, ok := watchers[id]
				if !ok {
					wl = map[*websocket.Conn]None{}
					watchers[id] = wl
				}
				wl[r.Ws] = None{}
			}

			//最近のもので送ってないものがあったら送る
			adddateLatestPosts.Do(func(v interface{}){
				l, ok := v.(*LatestLog)
				if ok && l.AddDate > r.Origin {
					wl, ok := watchers[l.Parent.Hex()]
					if !ok {
						return
					}
					_, ok = wl[r.Ws]
					if !ok {
						return
					}

					websocket.Message.Send(r.Ws, string(l.Json))
				}
			})
		case *RemoveRequest:
			for id, wl := range watchers {
				delete(wl, r.Ws)
				if len(wl) == 0 {
					delete(watchers, id)
				}
			}
		case *BroadcastRequest:
			data, _, err := websocket.JSON.Marshal(r.NodeData)
			if err != nil {
				log.Println("broadcast error: ",err)
				continue
			}

			adddateLatestPosts.Value = &LatestLog{
				Json: data,
				Parent: r.NodeData.Parent,
				AddDate: r.NodeData.AddDate,
			}
			adddateLatestPosts = adddateLatestPosts.Next()

			wl, ok := watchers[r.NodeData.Parent.Hex()]
			if ok {
				id := r.NodeData.Id.Hex()
				wln, ok := watchers[id]
				if !ok {
					wln = map[*websocket.Conn]None{}
					watchers[id] = wln
				}
				for ws, _ := range wl {
					websocket.Message.Send(ws, string(data))
					wln[ws] = None{}
				}
			}
		case *GetLatestRequest:
			adddateLatestPosts.Do(func(v interface{}){
				log, ok := v.(*LatestLog)
				if ok && log.AddDate > r.Origin {
					r.Result <- log
				}
			})
			r.Result <- &LatestLog{}
		}
	}
}
