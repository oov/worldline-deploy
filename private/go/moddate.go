package main

import(
	"log"
	"net/http"
	"code.google.com/p/go.net/websocket"
	"strconv"
)

type ModDateReceiveData struct {
	World string `json:"world"`
}

type ModDateAddRequest struct {
	Ws *websocket.Conn
	World string
}

type ModDateRemoveRequest struct {
	Ws *websocket.Conn
}

type ModDateBroadcastRequest struct {
	NodeData NodeDocument
}

type ModDateGetLatestRequest struct {
	World string
	Result chan<- int64
}

type ModDateWatchers struct {
	watchers map[*websocket.Conn]None
	moddate int64
}

var (
	moddateRequest = make(chan interface{}, 100)
)

func latest(ws *websocket.Conn) {
	defer func(){
		moddateRequest <- &ModDateRemoveRequest{
			Ws: ws,
		}
	}()

	var err error
	var r = ModDateReceiveData{}

	for {
		err = websocket.JSON.Receive(ws, &r)
		if err != nil {
			break
		}
		moddateRequest <- &ModDateAddRequest{
			Ws: ws,
			World: r.World,
		}
	}
}

func latestajax(w http.ResponseWriter, req *http.Request){
	r := make(chan int64)
	moddateRequest <- &ModDateGetLatestRequest{
		World: req.FormValue("world"),
		Result: r,
	}

	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("X-XSS-Protection", "1; mode=block")
	w.Header().Set("X-Frame-Options", "SAMEORIGIN")
	w.Header().Set("X-Content-Security-Policy", "allow 'self' " + *domain)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	var origin string
	if *certFile == "" || *keyFile == "" {
		origin = "http"
	} else {
		origin = "https"
	}
	w.Header().Set("Access-Control-Allow-Origin", origin + "://" + *domain)
	w.Header().Set("Access-Control-Allow-Methods", "GET")
	w.Header().Set("Access-Control-Allow-Headers", "*")

	w.Write([]byte(strconv.FormatInt(<-r, 10)))
}

func manageModDateWatchers() {
	watchers := map[string]*ModDateWatchers{
		"": &ModDateWatchers{watchers: map[*websocket.Conn]None{}},
	}

	for {
		switch r := (<-moddateRequest).(type) {
		case *ModDateAddRequest:
			wl, ok := watchers[r.World]
			if !ok {
				wl = &ModDateWatchers{
					watchers: map[*websocket.Conn]None{},
				}
				watchers[r.World] = wl
			}
			wl.watchers[r.Ws] = None{}

		case *ModDateRemoveRequest:
			for world, wl := range watchers {
				delete(wl.watchers, r.Ws)
				if len(wl.watchers) == 0 && world != "" {
					delete(watchers, world)
				}
			}

		case *ModDateBroadcastRequest:
			data, _, err := websocket.JSON.Marshal(r.NodeData.ModDate)
			if err != nil {
				log.Println("broadcast error: ",err)
				continue
			}

			wl, ok := watchers[r.NodeData.Parent.Hex()]
			if ok {
				wl.moddate = r.NodeData.ModDate
				for ws, _ := range wl.watchers {
					websocket.Message.Send(ws, string(data))
				}
			}
		case *ModDateGetLatestRequest:
			wl, ok := watchers[r.World]
			if !ok {
				r.Result <- 0
				continue
			}

			r.Result <- wl.moddate
		}
	}
}
