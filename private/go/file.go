package main

import (
	"net/http"
)

func file(w http.ResponseWriter, r *http.Request){
	filename := r.URL.Path[2:]
	gf, err := gfs.Open(filename)
	if err != nil {
		if err.Error() == "Document not found" {
			http.NotFound(w, r)
		} else {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}
	defer gf.Close()

	http.ServeContent(w, r, filename, gf.UploadDate(), gf)
/* キャッシュ機構作る時に使えるコードになると思うのでとっておく
	dir := *fcgiRoot + filename[0:2] + "/" + filename[2:4] + "/";
	err = os.MkdirAll(dir, os.ModePerm)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	f, err := os.Create(dir + filename[4:])
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	_, err = io.Copy(f, gf)
	if err != nil {
		f.Close()
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = f.Close()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.ServeFile(w, r, dir + filename[4:])
*/
}

