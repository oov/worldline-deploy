package main

import(
	"flag"
	"log"
	"fmt"
	"net"
	"net/http"
	"net/http/fcgi"
	"code.google.com/p/go.net/websocket"
	"launchpad.net/mgo"
	"launchpad.net/mgo/bson"
	"strings"
	"os"
	"time"
)

type NodeDocument struct {
	Id bson.ObjectId `bson:"_id" json:"_id"`
	Parent bson.ObjectId `bson:"parent" json:"parent"`
	User UserDocument `bson:"user" json:"user"`
	Title string `bson:"title" json:"title"`
	File string `bson:"file,omitempty" json:"file,omitempty"`
	FileExt string `bson:"file_ext,omitempty" json:"file_ext,omitempty"`
	Del int64 `bson:"del,omitempty" json:"del,omitempty"`
	Ninja int64 `bson:"ninja,omitempty" json:"ninja,omitempty"`
	AddDate int64 `bson:"adddate" json:"adddate"`
	ModDate int64 `bson:"moddate" json:"moddate"`
}

type UserDocument struct {
	Id bson.ObjectId `bson:"_id" json:"_id"`
	Name string `bson:"name" json:"name"`
	Avatar int64 `bson:"avatar" json:"avatar"`
}

type None struct {}

var (
	domain *string = flag.String("domain", "", "WORLDLINE running domain name")
	dbName *string = flag.String("db", "worldline", "Database name")
	port *int = flag.Int("p", 57102, "Port to listen")
	certFile *string = flag.String("cert", "", "Cert file")
	keyFile *string = flag.String("key", "", "Key file")
	fcgiAddr *string = flag.String("fcgi", "unix:///tmp/worldline.sock", "FCGI address")
	//fcgiRoot *string = flag.String("fcgiroot", "", "FCGI file server root")
)

var session *mgo.Session
var db *mgo.Database
var gfs *mgo.GridFS
var tree *mgo.Collection
var users *mgo.Collection

func main(){
	flag.Parse()
	if *domain == "" {
		flag.Usage()
		return
	}

	var err error
	session, err = mgo.Dial("localhost")
	if err != nil {
		log.Fatalln("mgo.Dial: ", err)
	}
	defer session.Close()

	db = session.DB(*dbName)
	gfs = db.GridFS("file")
	tree = db.C("tree")
	users = db.C("users")

	var read int64

	//新着記事データを埋めておく
	var node NodeDocument
	q := tree.Find(nil).Sort(bson.M{"adddate": -1}).Limit(100)
	iter := q.Iter()
	for iter.Next(&node) {
		if node.Del != 0 {
			node.Title = ""
			node.File = ""
			node.FileExt = ""
		}
		if node.Ninja != 0 || node.Del != 0 {
			node.User = UserDocument{}
		} else {
			var user UserDocument
			users.Find(bson.M{"_id":node.User.Id}).One(&user)
			node.User.Avatar = user.Avatar
		}
		data, _, err := websocket.JSON.Marshal(node)
		if err != nil {
			log.Println("broadcast error: ",err)
			continue
		}
		adddateLatestPosts = adddateLatestPosts.Prev()
		adddateLatestPosts.Value = &LatestLog{
			Json: data,
			Parent: node.Parent,
			AddDate: node.AddDate,
		}
		if node.AddDate > read {
			read = node.AddDate
		}
	}

	go manageAddDateWatchers()
	go manageModDateWatchers()
	go func(){
		var node NodeDocument
		for {
			var readOld = read
			q := tree.Find(bson.M{"adddate": bson.M{"$gt":read}})
			q.Sort(bson.M{"adddate": 1})
			iter := q.Iter()
			for iter.Next(&node) {
				if node.Del != 0 {
					node.Title = ""
					node.File = ""
					node.FileExt = ""
				}
				if node.Ninja != 0 || node.Del != 0 {
					node.User = UserDocument{}
				} else {
					var user UserDocument
					users.Find(bson.M{"_id":node.User.Id}).One(&user)
					node.User.Avatar = user.Avatar
				}
				adddateRequest <- &BroadcastRequest{
					NodeData: node,
				}
				if node.AddDate > read {
					read = node.AddDate
				}
			}
			if iter.Err() != nil {
		    log.Println(iter.Err())
			}

			//もし変更があった場合はグローバルに対するModDateの変更を調べる
			if readOld != read {
				q := tree.Find(bson.M{"parent": nil, "moddate": bson.M{"$gt": readOld}})
				q.Sort(bson.M{"moddate": -1})
				iter := q.Iter()
				for iter.Next(&node) {
					moddateRequest <- &ModDateBroadcastRequest{
						NodeData: node,
					}
					if node.AddDate > read {
						read = node.AddDate
					}
				}
				if iter.Err() != nil {
			    log.Println(iter.Err())
				}
			}
			time.Sleep(time.Second)
		}
	}()

	http.Handle("/", websocket.Handler(watcher))
	http.HandleFunc("/ax", ajax)
	http.Handle("/latest", websocket.Handler(latest))
	http.HandleFunc("/latestax", latestajax)

	go func(){
		sm := http.NewServeMux()
		sm.HandleFunc("/", file)
		err := listenAndServeFcgi(*fcgiAddr, sm)
		log.Println("listenAndServeFcgi: ", err)
	}()

	if *certFile == "" || *keyFile == "" {
		err = http.ListenAndServe(fmt.Sprintf(":%d", *port), nil)
	} else {
		err = http.ListenAndServeTLS(fmt.Sprintf(":%d", *port), *certFile, *keyFile, nil)
	}
	if err != nil {
		log.Fatalln("ListenAndServe(or ListenAndServeTLS): ", err)
	}
}

func listenAndServeFcgi(server string, serveMux *http.ServeMux) (err error) {
	var s string
	var l net.Listener

	if strings.HasPrefix(server, "unix://") {
		s = (server)[7:]
		os.Remove(s)
		l, err = net.Listen("unix", s)
		os.Chmod(s, 0666)
	} else if strings.HasPrefix(server, "tcp://") {
		s = (server)[6:]
		l, err = net.Listen("tcp", s)
	} else {
		err = fmt.Errorf("unknown server bind address")
	}

	if err != nil {
		return
	}
	return fcgi.Serve(l, serveMux)
}
